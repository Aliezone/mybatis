import com.aftery.dao.IUserDao;
import com.aftery.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.jdbc.SQL;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.jws.soap.SOAPBinding;
import javax.naming.ldap.PagedResultsControl;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-12下午4:04
 * @Version 1.0
 **/
@SuppressWarnings("all")
public class AnnotationCRUD {
    private InputStream inputStream;
    private SqlSession sqlSession;
    private SqlSessionFactory factory;
    private  IUserDao mapper;

    @Before
    public void init() throws IOException {
        inputStream=Resources.getResourceAsStream("SqlMapConfig.xml");
        factory=new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession=factory.openSession();
         mapper = sqlSession.getMapper(IUserDao.class);
    }
    @After
    public void aftery() throws IOException {
        sqlSession.commit();
        sqlSession.close();
        inputStream.close();
    }

    @Test
    public void saveUserTest(){
        User user=new User();
        user.setAddress("西安");
        user.setBirthday(new Date());
        user.setSex("女");
        user.setUsername("张三");
        mapper.saveUser(user);
    }
    @Test
    public void update(){
        User user=new User();
        user.setAddress("西安1");
        user.setBirthday(new Date());
        user.setSex("男");
        user.setUsername("张三");
        user.setId(52);
        mapper.updateUserBy(user);
    }
    @Test
    public void deleteUserTest(){
       mapper.delete(52);
    }
    @Test
    public void findByIdTest(){
        User byId = mapper.findById(48);
        System.out.println(byId);
    }

    @Test
    public void FindByNameTest(){
        /*List<User> byName = mapper.findByName("%王%");*/
        List<User> byName = mapper.findByName("王");
        for(User user:byName){
            System.out.println(user);
        }
    }

    @Test
    public void countTest(){
        int count = mapper.count();
        System.out.println(count);
    }

}
