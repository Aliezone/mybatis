
import com.aftery.dao.IUserDao;
import com.aftery.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-12下午3:21
 * @Version 1.0
 **/
@SuppressWarnings("all")
public class AnnotationMybatisTest {
    private InputStream inputStream;
    private SqlSession sqlSession;
    private  IUserDao mapper;
    @Before
    public  void before() throws IOException {
        inputStream=Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession=factory.openSession();
         mapper = sqlSession.getMapper(IUserDao.class);
    }

    @After
    public void  after() throws IOException {
        sqlSession.close();
        inputStream.close();
    }

    @Test
    public void findAllTest(){
        List<User> all = mapper.findAll();
        all.forEach(System.out::println);
    }
}
