package com.aftery.dao;

import com.aftery.domain.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-12下午3:14
 * @Version 1.0
 **/
@SuppressWarnings("all")
public interface IUserDao {

    /**
     * 查询所有用户
     * @return
     */
    @Select("select  * from user ")
    List<User> findAll();

    /**
     * 保存操作
     * @param user
     */
    @Insert("insert into  user (username,sex,address,birthday) values (#{username},#{sex},#{address},#{birthday})")
    void saveUser(User user);

    /**
     * 更新操作
     * @param user
     */
    @Update("update user set username=#{username},sex=#{sex},address=#{address},birthday=#{birthday} where id=#{id}")
    void  updateUserBy(User user);

    /**
     * 删除操作
     */
    @Delete("Delete from  user where  id=#{id}")
    void  delete(Integer id);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @Select("select  * from user where id=#{id}")
    User findById(Integer id);

    /**
     * 模糊查询
     * @param name
     * @return
     */
    /*@Select("select * from user where  username like #{username} ")*/
    @Select("select * from user where  username like '%${value}%' ")
    List<User> findByName(String name);

    @Select("select COUNT(*) from  user")
    int count();
}
