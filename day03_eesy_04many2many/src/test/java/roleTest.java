import com.aftery.dao.IRoleDao;
import com.aftery.dao.IUserDao;
import com.aftery.domain.Role;
import com.aftery.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11下午3:37
 * @Version 1.0
 **/
@SuppressWarnings("all")
public class roleTest {
    private InputStream in;
    private SqlSession sqlSession;
    private IRoleDao dao;

    @Before//用于在测试方法执行之前执行
    public void init() throws Exception {
        //1.读取配置文件，生成字节输入流
        in = Resources.getResourceAsStream("mybatisConfig.xml");
        //2.获取SqlSessionFactory
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        //3.获取SqlSession对象
        sqlSession = factory.openSession(true);
        //4.获取dao的代理对象
        dao = sqlSession.getMapper(IRoleDao.class);
    }

    @After//用于在测试方法执行之后执行
    public void destroy() throws Exception {
        //提交事务
        // sqlSession.commit();
        //6.释放资源
        sqlSession.close();
        in.close();
    }

    @Test
    public  void testFndAll(){
        List<Role> all = dao.findAll();
        all.forEach(System.out::println);
    }

}
