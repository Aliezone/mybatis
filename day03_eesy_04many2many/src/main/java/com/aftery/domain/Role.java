package com.aftery.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11下午3:32
 * @Version 1.0
 **/
@SuppressWarnings("all")
@Getter
@Setter
@ToString
public class Role implements Serializable {
    private Integer roleId;
    private String roleName;
    private String roleDesc;
    private List<User> users;
}
