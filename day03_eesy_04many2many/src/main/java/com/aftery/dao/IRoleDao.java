package com.aftery.dao;

import com.aftery.domain.Role;

import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11下午3:33
 * @Version 1.0
 **/
public interface IRoleDao {
    /**
     * 查询所有角色
     * @return
     */
    List<Role> findAll();
}
