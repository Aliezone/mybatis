package com.aftery.test;

import com.aftery.dao.IUserDao;
import com.aftery.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * @author 黑马程序员
 * @Company http://www.ithiema.com
 */
public class UserTest {

    private InputStream in;
    private SqlSession sqlSession;
    private IUserDao userDao;
    private SqlSessionFactory factory;

    @Before//用于在测试方法执行之前执行
    public void init()throws Exception{
        //1.读取配置文件，生成字节输入流
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.获取SqlSessionFactory
         factory = new SqlSessionFactoryBuilder().build(in);
        //3.获取SqlSession对象
        sqlSession = factory.openSession(true);
        //4.获取dao的代理对象
        userDao = sqlSession.getMapper(IUserDao.class);
    }

    @After//用于在测试方法执行之后执行
    public void destroy()throws Exception{
        //提交事务
        // sqlSession.commit();
        //6.释放资源
        sqlSession.close();
        in.close();
    }

    /**
     * 测试一级缓存
     */
    @Test
   public  void TestFirstLeveCache(){
       User user = userDao.findById(45);
       user.getUser(true);
       //关闭sqlsession对象
      /* sqlSession.close();
       //重新打开sqlsession
       sqlSession=factory.openSession(true);
        userDao = sqlSession.getMapper(IUserDao.class);*/
      //直接清空缓存
        sqlSession.clearCache();
       User user1 = userDao.findById(45);
       user.getUser(true);
       System.out.println(user==user1);
   }

    /**
     * 测试缓存的同步
     * mybatis一级缓存调用sqlsession的修改，添加，删除，commit()，close()等方法时，会清空一级缓存
     */
    @Test
   public  void testClearCache(){
       User user = userDao.findById(45);
       user.getUser(true);

       User user2=new User();
       user2.setId(45);
       user2.setAddress("北京");
       user2.setUsername("update user");
        userDao.updateUserBy(user2);

       User user1 = userDao.findById(45);
       user.getUser(true);
       System.out.println(user==user1);
   }


}
