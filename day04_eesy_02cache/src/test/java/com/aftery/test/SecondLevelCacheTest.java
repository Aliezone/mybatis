package com.aftery.test;

import com.aftery.dao.IUserDao;
import com.aftery.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * @author 黑马程序员
 * @Company http://www.ithiema.com
 */
public class SecondLevelCacheTest {

    private InputStream in;


    private SqlSessionFactory factory;

    @Before//用于在测试方法执行之前执行
    public void init() throws Exception {
        //1.读取配置文件，生成字节输入流
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.获取SqlSessionFactory
        factory = new SqlSessionFactoryBuilder().build(in);

    }

    @After//用于在测试方法执行之后执行
    public void destroy() throws Exception {
        //提交事务
        // sqlSession.commit();
        //6.释放资源
        in.close();
    }

    /**
     * 测试二级级缓存
     */
    @Test
    public void TestFirstLeveCache() {
        SqlSession sqlSession = factory.openSession();
        IUserDao dao = sqlSession.getMapper(IUserDao.class);
        User user = dao.findById(45);
        user.getUser(true);
        sqlSession.close();

        SqlSession sqlSession1 = factory.openSession();
        IUserDao dao1 = sqlSession1.getMapper(IUserDao.class);
        User user1 = dao1.findById(45);
        user1.getUser(true);
        //sqlSession1.close();
        System.out.println(user == user1);
    }
    /**
     * 测试二级级缓存
     */
    @Test
    public void TestFirstLeveCache1() {
        SqlSession sqlSession = factory.openSession();
        IUserDao dao = sqlSession.getMapper(IUserDao.class);
        List<User> user = dao.findAll();
        user.forEach(System.out::println);
        sqlSession.close();

        SqlSession sqlSession1 = factory.openSession();
        IUserDao dao1 = sqlSession1.getMapper(IUserDao.class);
        List<User> user1 = dao1.findAll();
       user1.forEach(System.out::println);
        //sqlSession1.close();
        System.out.println(user == user1);
    }


}
