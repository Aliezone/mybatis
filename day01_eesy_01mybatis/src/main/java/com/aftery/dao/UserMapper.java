package com.aftery.dao;

import com.aftery.domain.User;

import java.util.List;

/**
 * @ClassName UserMapper
 * @Description TODO
 * @Author aftery
 * @Date 19-4-24下午11:08
 * @Version 1.0
 **/
public interface UserMapper {

    List<User> findAll();
}
