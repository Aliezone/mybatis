package com.aftert.test;

import com.aftery.dao.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @ClassName UserTest
 * @Description TODO
 * @Author aftery
 * @Date 19-4-24下午11:16
 * @Version 1.0
 **/
public class UserTest {
    public static void main(String[] args) throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("mybatisConfig.xml");
        SqlSessionFactoryBuilder factoryBuilder = new SqlSessionFactoryBuilder();
        SqlSessionFactory sessionFactory = factoryBuilder.build(resourceAsStream);
        UserMapper mapper = sessionFactory.openSession().getMapper(UserMapper.class);
        mapper.findAll().forEach(System.out::println);
    }
}
