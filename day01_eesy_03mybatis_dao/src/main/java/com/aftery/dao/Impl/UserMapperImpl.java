package com.aftery.dao.Impl;

import com.aftery.dao.UserMapper;
import com.aftery.domain.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

/**
 * @ClassName UserMapperImpl
 * @Description TODO
 * @Author aftery
 * @Date 19-4-25下午11:26
 * @Version 1.0
 **/
public class UserMapperImpl implements UserMapper {
    private SqlSessionFactory factory;

    public UserMapperImpl(SqlSessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<User> findAll() {
        //1.使用工厂创建SqlSession对象
        SqlSession sqlSession = factory.openSession();
        //2.使用session执行查询所有方法
        List<User> list = sqlSession.selectList("com.aftery.dao.UserMapper.findAll");
        //3.返回查询结果
        return list;
    }

}
