package com.aftery.test;

import com.aftery.dao.Impl.UserMapperImpl;
import com.aftery.dao.UserMapper;
import com.aftery.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @ClassName UserTest
 * @Description TODO
 * @Author aftery
 * @Date 19-4-24下午11:16
 * @Version 1.0
 **/
public class UserTest {
    public static void main(String[] args) throws IOException {
        //读取配置文件
        InputStream resource = Resources.getResourceAsStream("mybatisConfig.xml");
        //创建sqlsessionFactory工厂
        SqlSessionFactoryBuilder builder=new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(resource);
        //使用工厂创建dao对象
        UserMapper mapper=new UserMapperImpl(factory);
        List<User> list = mapper.findAll();
        list.forEach(System.err::println);

    }

}
