package com.aftery.domain;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11下午4:37
 * @Version 1.0
 **/

public class QueryVo {
    private User user;

    @Override
    public String toString() {
        return "QueryVo{" +
                "user=" + user +
                '}';
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
