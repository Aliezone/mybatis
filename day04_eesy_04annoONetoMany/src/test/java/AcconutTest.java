import com.aftery.dao.IAcountDao;
import com.aftery.dao.IUserDao;
import com.aftery.domain.Account;
import com.aftery.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-12下午4:04
 * @Version 1.0
 **/
@SuppressWarnings("all")
public class AcconutTest {
    private InputStream inputStream;
    private SqlSession sqlSession;
    private SqlSessionFactory factory;
    private IAcountDao mapper;

    @Before
    public void init() throws IOException {
        inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        factory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = factory.openSession();
        mapper = sqlSession.getMapper(IAcountDao.class);
    }

    @After
    public void aftery() throws IOException {
        sqlSession.commit();
        sqlSession.close();
        inputStream.close();
    }

    @Test
    public void findAllTest() {
        List<Account> all = mapper.findAll();
        for (Account account:all){
            System.out.println("每个账号的信息");
            System.out.println(account);
        }
    }



}
