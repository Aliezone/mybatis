import com.aftery.dao.IAcountDao;
import com.aftery.dao.IUserDao;
import com.aftery.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-12下午5:22
 * @Version 1.0
 **/
@SuppressWarnings("all")
public class SecondLeveCatchTest {
    private InputStream inputStream;
    private SqlSession sqlSession;
    private SqlSessionFactory factory;
    private IUserDao mapper;

    @Before
    public void init() throws IOException {
        inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        factory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = factory.openSession();
        mapper = sqlSession.getMapper(IUserDao.class);
    }

    @After
    public void aftery() throws IOException {
        inputStream.close();
    }

    @Test
    public void findByIdTest() {
        User byId = mapper.findById(48);
        System.out.println(byId);
        sqlSession.close();          //释放一级缓存
        SqlSession sqlSession1=factory.openSession();
        IUserDao mapper1 = sqlSession1.getMapper(IUserDao.class);

        User byId1 = mapper1.findById(48);
        System.out.println(byId1);
        System.err.println(byId==byId1);

    }
}
