package com.aftery.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-12下午4:55
 * @Version 1.0
 **/
@Getter
@Setter
@ToString
@SuppressWarnings("all")
public class Account implements Serializable {
    private Integer id;
    private Integer uid;
    private Double money;
    private User user;

}
