package com.aftery.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.naming.ldap.PagedResultsControl;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-12下午3:11
 * @Version 1.0
 **/
@Getter
@Setter
@ToString
@SuppressWarnings("all")
public class User implements Serializable {
    private Integer userId;
    private String userName;
    private String userAddress;
    private String userSex;
    private Date userBirthday;
    private List<Account> accounts;
}
