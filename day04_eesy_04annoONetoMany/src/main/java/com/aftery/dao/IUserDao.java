package com.aftery.dao;

import com.aftery.domain.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-12下午3:14
 * @Version 1.0
 **/
@SuppressWarnings("all")
@CacheNamespace(blocking = true)
public interface IUserDao {

    /**
     * 查询所有用户
     *
     * @return
     */
    @Select("select  * from user ")
    @Results(id = "userMap", value = {
            @Result(id = true, column = "id", property = "userId"),
            @Result(property = "userAddress", column = "address"),
            @Result(property = "userBirthday", column = "birthday"),
            @Result(property = "userName", column = "username"),
            @Result(property = "userSex", column = "sex"),
            @Result(property = "accounts", column = "id", many = @Many(select = "com.aftery.dao.IAcountDao.findAccountByUid", fetchType = FetchType.LAZY))
    })
    List<User> findAll();


    /**
     * 根据id查询
     *
     * @param id
     * @return
     */

    @ResultMap("userMap")
    @Select("select  * from user where id=#{id}")
    User findById(Integer id);

    /**
     * 模糊查询
     *
     * @param name
     * @return
     */
    @ResultMap("userMap")
    @Select("select * from user where  username like #{username} ")
    List<User> findByName(String name);


}
