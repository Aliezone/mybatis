package com.aftery.test;

import com.aftery.dao.IUserDao;
import com.aftery.domain.QueryVo;
import com.aftery.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;


public class MybatisTest {
    private Logger logger=Logger.getLogger(MybatisTest.class);
    private InputStream in;
    private SqlSession sqlSession;
    private IUserDao dao;

    @Before //用于测试前执行
    public  void init() throws Exception{
        //1:读取配置文件，字节输出流
        in = Resources.getResourceAsStream("mybatisConfig.xml");
        //2:获取sqlsessionfactory
        SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(in);
        //3:获取sqlsession对象
         sqlSession = sqlSessionFactory.openSession();
        //4:获取dao的代理对象
         dao = sqlSession.getMapper(IUserDao.class);

    }
    @After   //用于在测试方法之后执行
    public  void destroy() throws Exception{
        sqlSession.commit();
        sqlSession.close();
        in.close();
    }
    @Test
    public  void testFindAll(){
        //5:执行查询所有方法
        List<User> list = dao.findAll();
        list.forEach(user ->logger.error(user));
    }
    @Test
    public void testSave(){
        User user = new User();
        user.setUserName("modify User property");
        user.setUserAddress("北京市顺义区");
        user.setUserSex("男");
        user.setUserBirthday(new Date());
        System.out.println("保存操作之前："+user);
        //5.执行保存方法
        dao.saveUser(user);

        System.out.println("保存操作之后："+user);
    }

    /**
     * 测试更新操作
     */
    @Test
    public void testUpdate(){
        User user = new User();
        user.setUserId(49);
        user.setUserName("mybastis update user");
        user.setUserAddress("北京市顺义区");
        user.setUserSex("女");
        user.setUserBirthday(new Date());

        //5.执行保存方法
        dao.updateUser(user);
    }
    /**
     * 测试删除操作
     */
    @Test
    public void testDelete(){
        //5.执行删除方法
        dao.deleteUser(48);
    }
    /**
     * 测试删除操作
     */
    @Test
    public void testFindOne(){
        //5.执行查询一个方法
        User  user = dao.findById(46);
        logger.error(user);
    }
    @Test //测试模糊查询
    public  void findByName(){
        List<User> list = this.dao.findByName("%王%");
       /* List<User> list = dao.findByName("王");*/
        list.forEach(user -> logger.error(user));
    }
    /**
     * 测试查询总记录条数
     */
    @Test
    public void testFindTotal(){
        //5.执行查询一个方法
        int count = dao.findTotal();
        logger.error(count);
    }


    /**
     * 测试使用QueryVo作为查询条件
     */
    @Test
    public void testFindByVo(){
        QueryVo vo = new QueryVo();
        User user = new User();
        user.setUserName("%王%");
        vo.setUser(user);
        //5.执行查询一个方法
        List<User> users = dao.findUserByVo(vo);
        for(User u : users){
          logger.error(u);
        }
    }

}
