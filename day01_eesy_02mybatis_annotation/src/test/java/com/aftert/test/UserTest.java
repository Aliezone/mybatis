package com.aftert.test;

import com.aftery.dao.UserMapper;
import com.aftery.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @ClassName UserTest
 * @Description TODO
 * @Author aftery
 * @Date 19-4-24下午11:16
 * @Version 1.0
 **/
public class UserTest {

    SqlSessionFactory sessionFactory=null;
    @Before
    public  void  before() throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("mybatisConfig.xml");
        SqlSessionFactoryBuilder factoryBuilder = new SqlSessionFactoryBuilder();
        sessionFactory= factoryBuilder.build(resourceAsStream);
    }

    @Test
    public void find() throws IOException {
        UserMapper mapper = sessionFactory.openSession().getMapper(UserMapper.class);
        mapper.findAll().forEach(System.out::println);
    }
    @Test
    public  void  findbyId(){
        UserMapper mapper = sessionFactory.openSession().getMapper(UserMapper.class);
        User user =null;
        int i=1;
        while (null==user){
            i++;
            user= mapper.findbyId(i);
        }
        System.out.println(user);
    }
}
