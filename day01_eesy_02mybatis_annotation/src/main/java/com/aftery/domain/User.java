package com.aftery.domain;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @ClassName User
 * @Description TODO
 * @Author aftery
 * @Date 19-4-24下午11:06
 * @Version 1.0
 **/
@Data
@ToString
public class User {
    private Integer id;
    private  String userName;
    private Date birthday;
    private String sex;
    private String address;

}
