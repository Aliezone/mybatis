package com.aftery.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class QueryVo {
    private User user;
    private List<Integer> ids;
}
