package com.aftery.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11上午11:49
 * @Version 1.0
 **/
@Getter
@Setter
@ToString
public class User  implements Serializable {
    private Integer userId;
    private String userName;
    private String userAddress;
    private String userSex;
    private Date userBirthday;
}
