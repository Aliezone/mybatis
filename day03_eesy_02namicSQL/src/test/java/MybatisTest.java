import com.aftery.dao1.IUserDao;
import com.aftery.domain.QueryVo;
import com.aftery.domain.User;
import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11上午11:57
 * @Version 1.0
 **/
@SuppressWarnings("all")
public class MybatisTest {

    private InputStream in;
    private SqlSession sqlSession;
    private IUserDao userDao;

    @Before//用于在测试方法执行之前执行
    public void init() throws Exception {
        //1.读取配置文件，生成字节输入流
        in = Resources.getResourceAsStream("mybatisConfig.xml");
        //2.获取SqlSessionFactory
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        //3.获取SqlSession对象
        sqlSession = factory.openSession(true);
        //4.获取dao的代理对象
        userDao = sqlSession.getMapper(IUserDao.class);
    }

    @After//用于在测试方法执行之后执行
    public void destroy() throws Exception {
        //提交事务
        // sqlSession.commit();
        //6.释放资源
        sqlSession.close();
        in.close();
    }

    /**
     * 测试查询所有
     */
    @Test
    public void testfindAll() {
        //5.执行查询所有方法
        List<User> users = userDao.findAll();
        for (User user : users) {
            System.out.println(user);

        }

    }

    /**
     * 测试根据id查询
     */
    @Test
    public void testFindOne() {
        //5.执行查询一个方法
        User user = userDao.findById(50);
        System.out.println(user);
    }

    /**
     * 测试模糊查询操作
     */
    @Test
    public void testFindByName() {
        //5.执行查询一个方法
        List<User> users = userDao.findByName("%王%");
        /* List<User> users = userDao.findByName("王");*/
        for (User user : users) {
            System.out.println(user);
        }
    }


    /**
     * 测试使用QueryVo作为查询条件
     */
    @Test
    public void testFindByVo() {
        QueryVo vo = new QueryVo();
        User user = new User();
        user.setUserName("%王%");
        vo.setUser(user);
        //5.执行查询一个方法
        List<User> users = userDao.findUserByVo(vo);
        for (User u : users) {
            System.out.println(u);
        }
    }
    /**
     * 根据条件查询
     */
    @Test
    public  void testfindUserByCondition(){
        User user=new User();
        user.setUserAddress("北京");
        user.setUserId(41);
        List<User> userByCondition = userDao.findUserByCondition(user);
        userByCondition.forEach(System.out::println);
    }

    /**
     * 根据queryvo中的Id集合实现查询用户列表
     */
    @Test
    public  void testFindUserInIds(){
        QueryVo vo=new QueryVo();
        List<Integer> list= new ArrayList<>();
        list.add(41);
        list.add(42);
        list.add(43);
        vo.setIds(list);
        List<User> userInIds = userDao.findUserInIds(vo);
        userInIds.forEach(System.out::println);
    }
}
