package com.aftery.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11下午2:47
 * @Version 1.0
 **/
@Getter
@Setter
@ToString
@SuppressWarnings("all")
public class AccountUser extends  Account{
    private String username;
    private String address;
}
