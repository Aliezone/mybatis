package com.aftery.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11下午2:47
 * @Version 1.0
 **/
@Getter
@Setter
@ToString
@SuppressWarnings("all")
public class User {
    private Integer id;
    private String username;
    private String address;
    private String sex;
    private Date birthday;
}
