package com.aftery.dao;

import com.aftery.domain.Account;
import com.aftery.domain.AccountUser;

import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11下午2:49
 * @Version 1.0
 **/
@SuppressWarnings("all")
public interface IAccountDao {

    /**
     * 查询所有账户，同时还要获取到当前账户的所属用户信息
     * @return
     */
    List<Account> findAll();

    /**
     * 查询所有账户，并且带有用户名称和地址信息
     * @return
     */
    List<AccountUser> findAllAccount();
}
