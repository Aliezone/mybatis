package com.aftery.dao.impl;

import com.aftery.dao.IUserDao;
import com.aftery.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11上午9:58
 * @Version 1.0
 **/
@SuppressWarnings("all")
public class UserDaoImpl implements IUserDao {

    private SqlSessionFactory factory;

    public UserDaoImpl(SqlSessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<User> findAll() {
        SqlSession sqlSession = factory.openSession();
        List<User> list = sqlSession.selectList("com.aftery.dao.IUserDao.findAll");
        sqlSession.close();
        return list;
    }


    @Override
    public void saveUser(User user) {
        SqlSession sqlSession = factory.openSession();
        int insert = sqlSession.insert("com.aftery.dao.IUserDao.saveUser", user);
        sqlSession.commit();
        sqlSession.close();
    }

    @Override
    public void updateUser(User user) {
        SqlSession sqlSession = factory.openSession();
        int insert = sqlSession.update("com.aftery.dao.IUserDao.updateUser", user);
        sqlSession.commit();
        sqlSession.close();
    }

    @Override
    public void deleteUser(Integer userId) {
        SqlSession sqlSession = factory.openSession();
        int insert = sqlSession.delete("com.aftery.dao.IUserDao.deleteUser", userId);
        sqlSession.commit();
        sqlSession.close();
    }

    @Override
    public User findById(Integer userId) {
        SqlSession sqlSession = factory.openSession();
       User user= sqlSession.selectOne("com.aftery.dao.IUserDao.findById", userId);
        sqlSession.close();
        return user;
    }

    @Override
    public List<User> findByName(String username) {
        SqlSession sqlSession = factory.openSession();
        List<User> list = sqlSession.selectList("com.aftery.dao.IUserDao.findByName", username);
        sqlSession.close();
       return list;
    }

    @Override
    public int findTotal() {
        SqlSession sqlSession = factory.openSession();
        int count= sqlSession.selectOne("com.aftery.dao.IUserDao.findTotal");
        sqlSession.close();
        return count;
    }
}
