package com.aftery.dao;

import com.aftery.domain.User;

import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11上午9:58
 * @Version 1.0
 **/
@SuppressWarnings("all")
public interface IUserDao {
    /**
     * 查询所有用户
     *
     * @return
     */
    List<User> findAll();

    /**
     * 保存用户
     *
     * @param user
     */
    void saveUser(User user);

    /**
     * 更新用户
     *
     * @param user
     */
    void updateUser(User user);

    /**
     * 根据Id删除用户
     *
     * @param userId
     */
    void deleteUser(Integer userId);

    /**
     * 根据id查询用户信息
     *
     * @param userId
     * @return
     */
    User findById(Integer userId);

    /**
     * 根据名称模糊查询用户信息
     *
     * @param username
     * @return
     */
    List<User> findByName(String username);

    /**
     * 查询总用户数
     *
     * @return
     */
    int findTotal();
}
