import com.aftery.dao.IUserDao;
import com.aftery.dao.impl.UserDaoImpl;
import com.aftery.domain.User;
import lombok.ToString;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-5-11上午10:17
 * @Version 1.0
 **/
@SuppressWarnings("all")
public class UserTest {
    private InputStream in;
    private IUserDao userDao;

    @Before//用于在测试方法执行之前执行
    public void init()throws Exception{
        //1.读取配置文件，生成字节输入流
        in = Resources.getResourceAsStream("mybatisConfig.xml");
        //2.获取SqlSessionFactory
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        //3.使用工厂对象，创建dao对象
        userDao = new UserDaoImpl(factory);
    }

    @After//用于在测试方法执行之后执行
    public void destroy()throws Exception{
        //6.释放资源
        in.close();
    }

    @Test
    public void  testfinAll(){
        List<User> all = userDao.findAll();
        all.forEach(System.out::println);
    }

    @Test
    public  void testSave(){
        User user = new User();
        user.setUsername("dao impl user1");
        user.setAddress("北京市顺义区1");
        user.setSex("女");
        user.setBirthday(new Date());
        System.out.println("保存操作之前："+user);
        userDao.saveUser(user);
        System.out.println("保存操作之后："+user);
    }
    @Test
    public void testUpdate(){
        User user = new User();
        user.setId(50);
        user.setUsername("dao update user1");
        user.setAddress("gongdong");
        user.setSex("女");
        user.setBirthday(new Date());
        userDao.updateUser(user);
    }
    @Test
    public void testdelete(){
        userDao.deleteUser(49);
    }
    @Test
    public void testfindByid(){
        User byId = userDao.findById(45);
        System.out.println(byId);
    }
    @Test
    public void testfindByName(){
        List<User> byName = userDao.findByName("%王%");
        byName.forEach(System.out::println);

    }
    @Test
    public void testCOunt(){
        int total = userDao.findTotal();
        System.out.println(total);
    }
}
