package com.aftery;

import com.aftery.domain.account;
import com.aftery.mapper.eesyMapper;
import com.aftery.utils.randomUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Random;

@SuppressWarnings("all")
public class accountTest {

    String resource = "mybatis.xml";
    InputStream inputStream = null;
    SqlSessionFactory factoryBuilder = null;

    @Before
    public void befa() {

        try {
            inputStream = Resources.getResourceAsStream(resource);
            factoryBuilder = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void selectTest() {
        try {
            long millis = System.currentTimeMillis();
            System.out.println(factoryBuilder + "------------------------------------------");
            SqlSession sqlSession = factoryBuilder.openSession();
            eesyMapper mapper = sqlSession.getMapper(eesyMapper.class);
            List<account> list = mapper.selectEesy();
            list.forEach(System.out::println);
            long millis1 = System.currentTimeMillis();
            System.out.println((millis1-millis)/1000+":秒");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addAccount() {
        int money = 30000;
        int count = 0;
        long timeMillis = System.currentTimeMillis();
        SqlSession sqlSession = factoryBuilder.openSession(ExecutorType.BATCH);
        while (money > 0) {
            System.out.println(factoryBuilder + "------------------------------------------");
            account account = new account();
            Random random = new Random();
            String getrandom = randomUtils.getrandom(random.nextInt(17));
            System.out.println(getrandom);
            account.setMoney((float) random.nextInt(10000));
            account.setName(getrandom);
            eesyMapper mapper = sqlSession.getMapper(eesyMapper.class);
            mapper.add(account);
            if ((count % 50) == 0) {
                sqlSession.flushStatements();
                sqlSession.commit();
            }
            count++;

            long millis = System.currentTimeMillis();
            if ((millis - timeMillis) > money) {
                money = -1;
            }
        }
        sqlSession.flushStatements();
        sqlSession.commit();
        long millis = System.currentTimeMillis();
        System.out.println("你一共添加了{" + count + "}:条数据,使用了：[" + (millis - timeMillis) / 1000 + "：秒]");
    }
}