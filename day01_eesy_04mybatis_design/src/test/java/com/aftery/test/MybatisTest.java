package com.aftery.test;


import com.aftery.dao.UserMapper;
import com.aftery.domain.User;
import com.aftery.mybatis.Sqlsession.SqlSession;
import com.aftery.mybatis.Sqlsession.SqlSessionFactory;
import com.aftery.mybatis.Sqlsession.SqlSessionFactoryBuilder;
import com.aftery.mybatis.io.Resources;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-4-28下午8:50
 * 12::
 * @Version 1.0
 **/
public class MybatisTest {


    public static void main(String[] args) {
        //读取配置文件
        InputStream stream = Resources.getResourceAsStream("mybatisConfig.xml");
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder=new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = sqlSessionFactoryBuilder.build(stream);
        System.out.println(factory+"{}{}{}");
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> list = mapper.findAll();
        list.forEach(System.out::println);

    }

    /**
     * 注解形式的测试
     */

    @Test
    public  void  annotations(){
        InputStream stream = Resources.getResourceAsStream("mybatisConfig.xml");
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder=new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = sqlSessionFactoryBuilder.build(stream);
        System.out.println(factory+"{}{}{}");
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> list = mapper.findAll();
        list.forEach(s-> System.err.println(s));
    }

}
