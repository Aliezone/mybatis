package com.aftery.mybatis.cfg;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-4-28下午8:42
 * 用于封装执行的sql语句和结果类型的权限类名
 * @Version 1.0
 **/
@Getter
@Setter
@ToString
public class Mapper {
    private String queryString;
    private  String resultType;
}
