package com.aftery.mybatis.Sqlsession;

import com.aftery.mybatis.Sqlsession.defults.DefaultSqlSessionFactory;
import com.aftery.mybatis.cfg.Configuration;
import com.aftery.mybatis.utils.XMLConfigBuilder;

import java.io.InputStream;

public class SqlSessionFactoryBuilder {

    public  SqlSessionFactory build(InputStream config){
        Configuration cfg= XMLConfigBuilder.loadConfiguration(config);
        System.out.println(cfg.getMappers());
        return  new DefaultSqlSessionFactory(cfg);
    }
}
