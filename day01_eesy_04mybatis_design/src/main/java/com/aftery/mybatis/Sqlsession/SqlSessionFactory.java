package com.aftery.mybatis.Sqlsession;

public interface SqlSessionFactory {
    /**
     *  用于打开一个新的sqlsession对象
     * @return
     */
    SqlSession openSession();
}
