package com.aftery.mybatis.Sqlsession.defults;

import com.aftery.mybatis.Sqlsession.SqlSession;
import com.aftery.mybatis.Sqlsession.SqlSessionFactory;
import com.aftery.mybatis.cfg.Configuration;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-4-28下午9:10
 * @Version 1.0
 **/
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private  Configuration cfg;
    public  DefaultSqlSessionFactory(Configuration cfg){
        this.cfg=cfg;
    }


    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(cfg);
    }
}
