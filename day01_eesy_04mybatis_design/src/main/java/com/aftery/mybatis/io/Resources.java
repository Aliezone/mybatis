package com.aftery.mybatis.io;

import java.io.InputStream;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-4-28下午8:49
 * @Version 1.0
 * 使用类加载起读取配置文件
 **/
public class Resources {

    /**
     * 根据传入的参数，获取一个字节输入流
     * @param filePath
     * @return
     */
    public static InputStream getResourceAsStream(String filePath){
        return Resources.class.getClassLoader().getResourceAsStream(filePath);
    }

}
