package com.aftery.mybatis.utils;

import com.aftery.mybatis.Sqlsession.SqlSessionFactory;
import com.aftery.mybatis.cfg.Configuration;


import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @Description TODO
 * @Author aftery
 * @Date 19-4-28下午9:01
 * @Version 1.0
 **/
public class DatasourceUtil {
    /**
     * 用于获取一个链接
     * @param cfg
     * @return
     */
    public static Connection getConnection(Configuration cfg) {
        Connection connection=null;
        try {
            Class.forName(cfg.getDriver());
            connection= DriverManager.getConnection(cfg.getUrl(), cfg.getUsername(), cfg.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }


}
