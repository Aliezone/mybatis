package com.aftery.dao;

import com.aftery.domain.User;
import com.aftery.mybatis.annotations.Select;

import java.util.List;

/**
 * @ClassName UserMapper
 * @Description TODO
 * @Author aftery
 * @Date 19-4-24下午11:08
 * @Version 1.0
 **/
public interface UserMapper {

    @Select("select * from user")
    List<User> findAll();
}
